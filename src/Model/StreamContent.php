<?php

namespace App\Model;

/**
 * StreamContent entity
 */
class StreamContent
{
    /**
     * @var string
     */
    protected $content;

    /**
     * @var string
     */
    protected $contentType;
    
    public function __construct(string $content, string $contentType) {
        $this->content = $content;
        $this->contentType = $contentType;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return self
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    public function getContentType(): string
    {
        return $this->contentType;
    }

    /**
     * @param string $contentType
     *
     * @return self
     */
    public function setContentType(string $contentType): self
    {
        $this->contentType = $contentType;

        return $this;
    }
}

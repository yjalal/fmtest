<?php

namespace App\Interfaces;

use App\Model\StreamContent;

/**
 * AbstractStreamContentProviderInterface Interface
 */
interface AbstractStreamContentProviderInterface
{
    public function fetchStreamContent(): StreamContent;
    public function parseImagesFromStreamContent(StreamContent $streamContent): array;
}
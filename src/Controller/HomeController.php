<?php

namespace App\Controller;

use App\Factory\StreamContentProviderFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /** @var StreamContentProviderFactory $streamContentProviderFactory */
    private $streamContentProviderFactory;
    
    /**
     * Constructor.
     *
     * @param StreamContentProviderFactory $streamContentProviderFactory
     */
    public function __construct(
        StreamContentProviderFactory $streamContentProviderFactory
    ) {
        $this->streamContentProviderFactory = $streamContentProviderFactory;
    }
    
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        $images = [];

        try {
            // Defining the stream content providers
            $streamContentProviders = [
                $this->streamContentProviderFactory->createStreamContentProvider(
                    $this->streamContentProviderFactory::RSS_STREAM_CONTENT_PROVIDER,
                    'http://www.commitstrip.com/en/feed/'
                ),
                $this->streamContentProviderFactory->createStreamContentProvider(
                    $this->streamContentProviderFactory::API_STREAM_CONTENT_PROVIDER,
                    'https://newsapi.org/v2/top-headlines?country=us&apiKey=c782db1cd730403f88a544b75dc2d7a0',
                    ['urlToImage']
                )
            ];
            
            // Fetch images from different stream content providers
            foreach ($streamContentProviders as $streamContentProvider) {
                $response = $streamContentProvider->fetchStreamContent();
                $images = array_merge(
                    $images,
                    $streamContentProvider->parseImagesFromStreamContent($response)
                );                
            }
            
            // Remove Duplicates Since the images array contains only urls            
            $images = array_unique($images);

        } catch (\Exception $exception) {
            // TODO
        }

        return $this->render('default/index.html.twig', ['images' => $images]);
    }
}
<?php

namespace App\Service;

use App\Interfaces\AbstractStreamContentProviderInterface;
use App\Model\StreamContent;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AbstractStreamContentProvider implements AbstractStreamContentProviderInterface
{
    /** @var HttpClient $httpClient */
    protected $httpClient;
    
    /** @var string $url */
    protected $url;
    
    /**
     * Constructor.
     */
    protected function __construct(string $url) {
        $this->url = $url;
        $this->httpClient = new HttpClient();
    }
    
    public function fetchStreamContent(): StreamContent
    {
        $response = $this->httpClient::create()->request(
            'GET',
            $this->url
        );
        
        if ($response->getStatusCode() !== 200) {
            throw new \Exception(
                "Failed to fetch response from the requested url."
            );
        }
        
        if (
            empty($response->getHeaders()) || 
            empty($response->getHeaders()['content-type'])
        ) {
            throw new \Exception(
                "Invalid content's response format."
            );
        }

        $contentType = $response->getHeaders()['content-type'][0];
        $content = $response->getContent();

        return new StreamContent($content, $contentType);
    }
    
    public function parseImagesFromStreamContent(
        StreamContent $content
    ): array
    {
        return [];
    }
}
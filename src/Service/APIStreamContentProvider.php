<?php

namespace App\Service;

use App\Model\StreamContent;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class APIStreamContentProvider extends AbstractStreamContentProvider 
{    
    /** @var array $imagesAttributes */
    protected $imagesAttributes;
    
    /**
     * Constructor.
     */
    public function __construct(string $url, array $imagesAttributes) {
        parent::__construct($url);
        $this->imagesAttributes = $imagesAttributes;
    }
    
    public function parseImagesFromStreamContent(
        StreamContent $content
    ): array
    {
        $images = [];
        $data = json_decode($content->getContent(), true);
        $this->findImageLinksRecursive($data, $images, $this->imagesAttributes);
        
        return $images;
    }
    
    public function findImageLinksRecursive(
        $value,
        array &$images,
        array $imagesAttributes = []
    ): void {
        $imageRegex = '/^(?:(?:https?|ftp):\/\/)?[\w.-]+(?:\/[\w\.-]*)+\.(?:[jpe?g|gif|png|svg]+)(?:\?[^\s]+)?$/'; // Adjusted regex
        if (is_array($value)) {
            foreach ($value as $subKey => $subValue) {
                if (
                    is_string($subValue) &&
                    !empty($imagesAttributes) &&
                    in_array($subKey, $imagesAttributes)
                ) {
                    $images[] = $subValue;
                    continue;
                }
                
                $this->findImageLinksRecursive($subValue, $images, $imagesAttributes);
            }
        }
        
        if (is_object($value)) {
            foreach ($value as $subKey => $subValue) {
                if (
                    is_string($subValue) &&
                    !empty($imagesAttributes) && 
                    in_array($subKey, $imagesAttributes)
                ) {
                    $images[] = $subValue;
                    continue;
                }
                
                $this->findImageLinksRecursive($subValue, $images, $imagesAttributes);
            }
        }
        
        if (is_string($value) && preg_match($imageRegex, $value)) {
            $images[] = $value;
        }
    }
}
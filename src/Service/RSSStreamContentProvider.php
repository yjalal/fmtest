<?php

namespace App\Service;

use App\Factory\AbstractStreamContentProviderInterface;
use App\Model\StreamContent;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RSSStreamContentProvider extends AbstractStreamContentProvider 
{    
    /**
     * Constructor.
     */
    public function __construct(string $url) {
        parent::__construct($url);
    }
    
    public function parseImagesFromStreamContent(
        StreamContent $content
    ): array
    {
        $images = [];
        $xmlDocument = new \DOMDocument();
        $xmlDocument->loadXML($content->getContent());
        $xpath = new \DOMXPath($xmlDocument);
        $nodes = $xpath->query('//item/content:encoded');
        
        foreach ($nodes as $node) {
            preg_match_all('~<img.*?src=["\']+(.*?)["\']+~', $node->nodeValue, $urls);
            if (!empty($urls) && !empty($urls[1])) {
                $images[] = $urls[1][0];
            }
        }
        
        return $images;
    }
}
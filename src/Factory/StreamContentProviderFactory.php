<?php

namespace App\Factory;

use App\Service\AbstractStreamContentProvider;
use App\Service\APIStreamContentProvider;
use App\Service\RSSStreamContentProvider;

/**
 * StreamContentProviderFactory Factory
 */
class StreamContentProviderFactory
{
    const RSS_STREAM_CONTENT_PROVIDER = 'rss';
    const API_STREAM_CONTENT_PROVIDER = 'api';
    
    public function createStreamContentProvider(
        string $type,
        string $url,
        array $imagesAttributes = []
    ): AbstractStreamContentProvider {
        switch ($type) {
            case self::RSS_STREAM_CONTENT_PROVIDER:
                return new RSSStreamContentProvider($url);
            case self::API_STREAM_CONTENT_PROVIDER:
                return new APIStreamContentProvider(
                    $url,
                    $imagesAttributes
                );
            default:
                throw new \InvalidArgumentException(
                    'Invalid stream content provider\'s type.'
                );
        }
    }
}
